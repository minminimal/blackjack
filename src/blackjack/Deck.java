/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.util.LinkedList;


public class Deck {
    private LinkedList<Card> cards;
    private int numCards;

    public Deck() {
        cards = new LinkedList<>();
    }

    public Deck(LinkedList<Card> cards, int numCards) {
        this();
        this.cards = cards;
        this.numCards = numCards;
    }

    public int getNumCards() {
        return numCards;
    }

    public void setNumCards(int numCards) {
        this.numCards = numCards;
    }

    public LinkedList<Card> getCards() {
        return cards;
    }

    public void setCards(LinkedList<Card> cards) {
        this.cards = cards;
    }
    
    public Card removeCard(int index) {
        numCards--;
        return cards.remove(index);
    }
    
    public void addDeck(LinkedList<Card> anotherDeck) {
        this.cards.addAll(anotherDeck);
        this.numCards += anotherDeck.size();
    }
}
