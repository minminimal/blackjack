/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

public class Blackjack {

    private Deck deck;
    private static final int BET = 10;

    private void runApp() {
        Scanner sc = new Scanner(System.in);
        System.out.println("****Welcome to Mindy's casino!!****");
        System.out.print("Please enter your name: ");
        String name = sc.nextLine();

        Player player = new Player(name, 100);
        System.out.println("Your chips: " + player.getChips());
        deck = createDeck();
        startGame(player);
    }

    private void startGame(Player player) {
        Scanner sc = new Scanner(System.in);

        while (deck.getNumCards() > 10 && (player.getChips() >= 10)) {
            System.out.println("Bet made: " + BET);

            LinkedList<Card> dealerCards = new LinkedList<>();
            LinkedList<Card> playerCards = new LinkedList<>();
            player.deductChips(BET);
            playerCards.add(dealCard());
            dealerCards.add(dealCard());
            playerCards.add(dealCard());
            dealerCards.add(dealCard());
            Hand hand = new Hand(playerCards, calculateTotalValue(playerCards), calculateSoftValue(playerCards), BET);
            player.addHand(hand);
            System.out.println("Dealer's Cards: " + dealerCards.getFirst().getValue() + " " + "*");

            if (checkBlackjack(playerCards)) {
                printPlayerCards(playerCards);
                if (checkBlackjack(dealerCards)) {
                    player.addChips(BET);
                    System.out.println("Push!");
                } else {
                    int winnings = (int) (hand.getBet() + (hand.getBet() * 1.5));
                    player.addChips(winnings);

                    System.out.println("Blackjack!!");

                }
            } else if (checkBlackjack(dealerCards)) {
                printPlayerCards(playerCards);
                System.out.println("Dealer Blackjack!!");
            } else {
                playerMove(player, player.getHands().get(0));
                System.out.println("Dealer's Cards: " + dealerCards.get(0).getValue() + " " + dealerCards.get(1).getValue());

                boolean playerBust = true;
                for (Hand myHand : player.getHands()) {
                    playerBust = isBust(myHand.getCards());
                    if (!playerBust) {
                        break;
                    }
                }

                if (playerBust) {
                    System.out.println("Dealer wins");
                } else {
                    int dealerValue = dealerMove(dealerCards);

                    for (Hand myHand : player.getHands()) {
                        int playerValue = myHand.getHigherValue();
                        if (myHand.getTotalValue() == 0) {
                            System.out.println("Surrendered!");
                            player.addChips(myHand.getBet() / 2);
                        } else if (playerValue > 21) {
                            System.out.println(player.getHands() + " Cards Bust!");
                        } else if (dealerValue > 21) {
                            System.out.println(player.getName() + " wins!");
                            player.addChips(myHand.getBet() * 2);
                        } else if (playerValue > dealerValue) {
                            System.out.println(player.getName() + " wins!");
                            player.addChips(myHand.getBet() * 2);
                        } else if (playerValue == dealerValue) {
                            System.out.println("Push!");
                            player.addChips(myHand.getBet());
                        } else {
                            System.out.println("Dealer wins!");
                        }
                    }
                }
            }
            player.resetHands();
            System.out.println("Your chips: " + player.getChips());
            System.out.println("----------------------------------------------");
        }

        if (deck.getNumCards() <= 10) {
            System.out.println("Less than 10 cards in game");
            System.out.println("Games ends!");
        } else if (player.getChips() < 10) {
            System.out.println("Not enough chips!");
            System.out.println("Games ends!");
        }
    }

    private void printDealerCards(LinkedList<Card> dealerCards) {
        System.out.print("Dealer Cards: ");
        for (Card card : dealerCards) {
            System.out.print(card.getValue() + " ");
        }
        System.out.println("");
    }

    private void printPlayerCards(LinkedList<Card> playerCards) {
        System.out.print("Your Cards: ");
        for (Card card : playerCards) {
            System.out.print(card.getValue() + " ");
        }
        System.out.println("");
    }

    private boolean checkBlackjack(LinkedList<Card> cards) {
        return calculateSoftValue(cards) == 21;
    }

    private void playerMove(Player player, Hand hand) {
        printPlayerCards(hand.getCards());
        Scanner sc = new Scanner(System.in);
        boolean isBust = false;
        boolean isDone = false;
        int round = 0;
        LinkedList<Card> playerCards = hand.getCards();
        while (!isBust && !isDone) {
            round++;
            int move = 0;
            System.out.println("Select moves: ");
            System.out.println("1. Hit");
            System.out.println("2. Stand");
            System.out.println("3. Double Down");
            System.out.println("4. Split");
            System.out.println("5. Surrender");
            System.out.print("> ");
            try {
                move = Integer.parseInt(sc.next());
            } catch (NumberFormatException ex) {
                System.out.println("Please enter a number!");
                continue;
            }

            if (move > 5 || move < 1) {
                System.out.println("Please enter a number between 1 and 5");
                continue;
            }

            if (move == 1) {
                playerCards.add(dealCard());
                hand.setTotalValue(calculateTotalValue(playerCards));
                printPlayerCards(playerCards);
                isBust = isBust(playerCards);
                if (isBust) {
                    return;
                }
            } else if (move == 2) {
                hand.setTotalValue(calculateTotalValue(playerCards));
                printPlayerCards(playerCards);
                return;
            } else if (move == 3) {
                if (round == 1) {
                    hand.addBet(BET);
                    player.deductChips(BET);
                    System.out.println("Bet: " + hand.getBet());
                    playerCards.add(dealCard());
                    hand.setTotalValue(calculateTotalValue(playerCards));
                    printPlayerCards(playerCards);
                    return;
                } else {
                    System.out.println("You can only double down on the first turn!");
                }

            } else if (move == 4) {
                if (round == 1 && player.getHands().size() == 1 && checkSameValue(playerCards)) {
                    player.deductChips(BET);
                    Card removeSecondCard = playerCards.remove(1);
                    LinkedList<Card> secondPlayerCards = new LinkedList<>();
                    secondPlayerCards.add(removeSecondCard);
                    Hand secondHand = new Hand(secondPlayerCards, numValueOfCard(removeSecondCard.getValue()), calculateSoftValue(secondPlayerCards), BET);
                    player.addHand(secondHand);
                    hand.addCard(dealCard());
                    playerMove(player, hand);
                    secondHand.addCard(dealCard());
                    playerMove(player, secondHand);
                    return;
                } else if (round != 1) {
                    System.out.println("You can only split on the first turn!");
                } else if (player.getHands().size() != 1) {
                    System.out.println("You can only split once!");
                } else {
                    System.out.println("Two cards must hold the same value!");
                }
            } else {
                hand.setTotalValue(0);
                printPlayerCards(playerCards);
                return;
            }
        }
    }

    private int dealerMove(LinkedList<Card> dealerCards) {
        int value = calculateTotalValue(dealerCards);
        int softValue = calculateSoftValue(dealerCards);
        while (value < 17 && softValue < 17) {
            dealerCards.add(dealCard());
            printDealerCards(dealerCards);
            value = calculateTotalValue(dealerCards);
        }
        return (value >= softValue) ? value : softValue;
    }

    private boolean isBust(LinkedList<Card> cards) {
        return calculateTotalValue(cards) > 21;
    }

    private boolean checkSameValue(LinkedList<Card> cards) {
        int value1 = numValueOfCard(cards.get(0).getValue());
        int value2 = numValueOfCard(cards.get(1).getValue());

        return value1 == value2;
    }

    private int calculateTotalValue(LinkedList<Card> cards) {
        int value = 0;
        for (Card card : cards) {
            value += numValueOfCard(card.getValue());
        }

        return value;
    }

    private Card dealCard() {
        return deck.removeCard(randomNumberGenerator(deck.getNumCards()));
    }

    private int randomNumberGenerator(int max) {
        Random rand = new Random();
        return rand.nextInt(max);
    }

    private Deck createDeck() {
        LinkedList<Card> cards = new LinkedList<>();
        String[] values = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
        for (Suit suit : Suit.values()) {
            for (String value : values) {
                cards.add(new Card(suit, value));
            }
        }
        return new Deck(cards, 52);
    }

    private int numValueOfCard(String value) {
        switch (value) {
            case "A":
                return 1;
            case "J":
            case "Q":
            case "K": {
                return 10;
            }
            default: {
                return Integer.parseInt(value);
            }
        }
    }

    private int calculateSoftValue(LinkedList<Card> cards) {
        int value = 0;
        boolean foundA = false;
        for (Card card : cards) {
            if (card.getValue().equals("A") && !foundA) {
                foundA = true;
                value += 11;
            } else {
                numValueOfCard(card.getValue());
            }
        }
        return value;
    }

    public static void main(String[] args) {
        Blackjack blackjack = new Blackjack();
        blackjack.runApp();
    }

}
