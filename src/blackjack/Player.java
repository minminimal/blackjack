/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.util.ArrayList;
import java.util.LinkedList;


public class Player {
    private String name;
    private int chips;
    private ArrayList<Hand> hands; 

    public Player() {
        hands = new ArrayList<>();
    }

    public Player(String name, int chips) {
        this();
        this.name = name;
        this.chips = chips;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getChips() {
        return chips;
    }

    public void setChips(int chips) {
        this.chips = chips;
    }

    public ArrayList<Hand> getHands() {
        return hands;
    }

    public void setHands(ArrayList<Hand> hands) {
        this.hands = hands;
    }
    
    public void resetHands() {
        this.hands.clear();
    }
    
    public void addHand(Hand hand) {
        this.hands.add(hand);
    }
    
    public void addChips(int chips) {
        this.chips += chips;
    }
    
    public void deductChips(int chips) {
        this.chips -= chips;
    }
}
