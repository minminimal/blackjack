/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.util.LinkedList;

/**
 *
 * @author mins
 */
public class Hand {

    private LinkedList<Card> cards;
    private int totalValue;
    private int softHandValue;
    private int bet;

    public Hand() {
        this.cards = new LinkedList<>();
    }

    public Hand(LinkedList<Card> cards, int totalValue, int softHandValue, int bet) {
        this();
        this.cards = cards;
        this.totalValue = totalValue;
        this.softHandValue = softHandValue;
        this.bet = bet;
    }

    public LinkedList<Card> getCards() {
        return cards;
    }

    public void setCards(LinkedList<Card> cards) {
        this.cards = cards;
    }

    public int getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(int totalValue) {
        this.totalValue = totalValue;
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public Card removeCard(int index) {
        return cards.remove(index);
    }

    public int getSoftHandValue() {
        return softHandValue;
    }

    public void setSoftHandValue(int softHandValue) {
        this.softHandValue = softHandValue;
    }

    public int getHigherValue() {
        if (softHandValue > 21) {
            return totalValue;
        } else {
            return (totalValue >= softHandValue) ? totalValue : softHandValue;
        }
    }
    
    public int getBet() {
        return bet;
    }
    
    public void setBet(int bet) {
        this.bet = bet;
    }
    
    public void addBet(int bet) {
        this.bet += bet;
    }
}
